package com.spring.app.models.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.spring.app.models.entity.Client;

@SuppressWarnings("unused")
public interface IClientDao extends PagingAndSortingRepository<Client, Long>{
	/*
	public List<Client> findAll();
	
	public void save(Client client);
	
	public Client findOne(Long id);
	
	public void delete(Long id);
*/
}
