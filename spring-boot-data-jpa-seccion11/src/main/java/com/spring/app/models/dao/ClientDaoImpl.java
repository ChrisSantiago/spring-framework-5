/*
package com.spring.app.models.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spring.app.models.entity.Client;

@Repository
public class ClientDaoImpl implements IClientDao {

	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	@Override
	public List<Client> findAll() {
		return em.createNativeQuery("select * from clients", Client.class).getResultList();
	}

	@Override
	@Transactional(readOnly = true)
	public Client findOne(Long id) {
		return em.find(Client.class, id);
	}

	@Override
	@Transactional
	public void save(Client client) {
		if (client.getId() != null && client.getId() > 0) {
			em.merge(client);
		} else {
			em.persist(client);
		}

	}

	@Override
	@Transactional
	public void delete(Long id) {
		em.remove(findOne(id));

	}

}
*/
package com.spring.app.models.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spring.app.models.entity.Client;

public class ClientDaoImpl  {



}

