package com.spring.app.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.spring.app.models.entity.Invoice;

public interface IInvoiceDao extends CrudRepository<Invoice, Long> {

}
