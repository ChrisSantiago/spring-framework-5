package com.spring.app.controllers;

import java.io.IOException;
import java.net.MalformedURLException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.spring.app.models.entity.Client;
import com.spring.app.models.service.IClientService;
import com.spring.app.models.service.IUploadFileService;
import com.spring.app.util.paginator.PageRender;

@Controller
@SessionAttributes("client")
public class ClientController {

	@Autowired
	private IClientService clientService;

	@Autowired
	private IUploadFileService uploadFileService;

	@GetMapping(value = "/uploads/{filename:.+}")
	public ResponseEntity<Resource> showPhoto(@PathVariable String filename) {

		Resource resource = null;
		try {
			resource = uploadFileService.load(filename);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
				.body(resource);
	}

	@GetMapping(value = "/show/{id}")
	public String show(@PathVariable(value = "id") Long id, Model model, RedirectAttributes flash) {
		Client client = clientService.findOne(id);
		if (client == null) {
			flash.addFlashAttribute("error", "The client does not exist in the date base");
			return "redirect:/list";
		}

		model.addAttribute("client", client);
		model.addAttribute("title", "Detail of client: " + client.getName());
		return "show-client";
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String list(@RequestParam(name = "page", defaultValue = "0") int page, Model model) {

		Pageable pageableRequest = PageRequest.of(page, 4);
		Page<Client> clients = clientService.findAll(pageableRequest);

		PageRender<Client> pageRender = new PageRender<>("/list", clients);
		model.addAttribute("title", "Client Lists");
		model.addAttribute("clients", clients);
		model.addAttribute("page", pageRender);
		return "list";
	}

	@RequestMapping(value = "/form")
	public String registerClient(Model model) {
		model.addAttribute("client", new Client());
		model.addAttribute("title", "Client Form");
		return "form";
	}

	@RequestMapping(value = "/form", method = RequestMethod.POST)
	public String save(@Valid Client client, BindingResult result, Model model,
			@RequestParam("file") MultipartFile photo, RedirectAttributes flash, SessionStatus status) {
		if (result.hasErrors()) {
			model.addAttribute("title", "Opps Error in the form");
			return "form";
		}

		if (!photo.isEmpty()) {

			if (client.getId() != null && client.getId() > 0 && client.getPhoto() != null
					&& client.getPhoto().length() > 0) {
				uploadFileService.delete(client.getPhoto());
			}

			String uniqueFilename = null;
			try {
				uniqueFilename = uploadFileService.copy(photo);
			} catch (IOException e) {
				e.printStackTrace();
			}

			flash.addFlashAttribute("info", "Success!! '" + uniqueFilename + "' has uploaded");

			client.setPhoto(uniqueFilename);

		}
		String messageflash = (client.getId() != null) ? "Success, The client has updated."
				: "Success, The client has created.";

		clientService.save(client);
		status.setComplete();
		flash.addFlashAttribute("success", messageflash);
		return "redirect:list";
	}

	@RequestMapping(value = "/form/{id}", method = RequestMethod.GET)
	public String update(@PathVariable("id") Long id, Model model, RedirectAttributes flash) {
		Client client = null;
		if (id > 0) {
			client = clientService.findOne(id);
			if (client == null) {
				flash.addFlashAttribute("error", "The client must exist in Database");
				return "redirect:/list";
			}
		} else {
			flash.addFlashAttribute("error", "The id client can not be 0");
			return "redirect:/list";
		}

		model.addAttribute("client", client);
		model.addAttribute("title", "Edit Client");

		return "form";
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public String delete(@PathVariable("id") Long id, RedirectAttributes flash) {
		if (id > 0) {
			Client client = clientService.findOne(id);
			clientService.delete(id);
			flash.addFlashAttribute("success", "Success, The client has deleted.");

			if (uploadFileService.delete(client.getPhoto())) {
				flash.addFlashAttribute("info", "Success, the photo: " + client.getPhoto() + " has deleted");
			}

		}
		return "redirect:/list";
	}

}
