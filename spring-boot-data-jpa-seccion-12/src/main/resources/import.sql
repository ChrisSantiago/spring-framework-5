INSERT INTO clients (id, name, lastname, email, create_at, photo)values (1,'Christofer', 'Santiago', 'chris@hotmail.com', '2020-05-11', '');
INSERT INTO clients (id, name, lastname, email, create_at, photo)values (2,'Jhonatan', 'Santiago', 'jhon@hotmail.com', '2016-05-11', '');
INSERT INTO clients (id, name, lastname, email, create_at, photo)values (3,'Monica', 'Santiago', 'jhon@hotmail.com', '2016-05-11', '');
INSERT INTO clients (id, name, lastname, email, create_at, photo)values (4,'Alejandro', 'Garcia', 'jhon@hotmail.com', '2016-05-11', '');
INSERT INTO clients (id, name, lastname, email, create_at, photo)values (5,'Mirian', 'Hernandez', 'jhon@hotmail.com', '2016-05-11', '');
INSERT INTO clients (id, name, lastname, email, create_at, photo)values (6,'Pedro', 'Santiago', 'jhon@hotmail.com', '2016-05-11', '');
INSERT INTO clients (id, name, lastname, email, create_at, photo)values (8,'Alicia', 'Santiago', 'jhon@hotmail.com', '2016-05-11', '');
INSERT INTO clients (id, name, lastname, email, create_at, photo)values (9,'Max', 'Lopez', 'jhon@hotmail.com', '2016-05-11', '');
INSERT INTO clients (id, name, lastname, email, create_at, photo)values (10,'Martin', 'Santiago', 'jhon@hotmail.com', '2016-05-11', '');
INSERT INTO clients (id, name, lastname, email, create_at, photo)values (11,'Kristen', 'Garcia', 'jhon@hotmail.com', '2016-05-11', '');
INSERT INTO clients (id, name, lastname, email, create_at, photo)values (12,'Erika', 'Santiago', 'jhon@hotmail.com', '2016-05-11', '');
INSERT INTO clients (id, name, lastname, email, create_at, photo)values (13,'Joel', 'Santiago', 'jhon@hotmail.com', '2016-05-11', '');
INSERT INTO clients (id, name, lastname, email, create_at, photo)values (14,'Armando', 'Santiago', 'jhon@hotmail.com', '2016-05-11', '');
INSERT INTO clients (id, name, lastname, email, create_at, photo)values (15,'Eduardo', 'Lopez', 'jhon@hotmail.com', '2016-05-11', '');
INSERT INTO clients (id, name, lastname, email, create_at, photo)values (16,'Mariana', 'Santiago', 'jhon@hotmail.com', '2016-05-11', '');
INSERT INTO clients (id, name, lastname, email, create_at, photo)values (17,'Alexa', 'Garcia', 'jhon@hotmail.com', '2016-05-11', '');
INSERT INTO clients (id, name, lastname, email, create_at, photo)values (18,'Luis', 'Hernandez', 'jhon@hotmail.com', '2016-05-11', '');
INSERT INTO clients (id, name, lastname, email, create_at, photo)values (19,'Karla', 'Lopez', 'jhon@hotmail.com', '2016-05-11', '');
INSERT INTO clients (id, name, lastname, email, create_at, photo)values (20,'Moncerrat', 'Santiago', 'jhon@hotmail.com', '2016-05-11', '');
INSERT INTO clients (id, name, lastname, email, create_at, photo)values (21,'Lucreci', 'Garcia', 'jhon@hotmail.com', '2016-05-11', '');
INSERT INTO clients (id, name, lastname, email, create_at, photo)values (22,'Alison', 'Hernandez', 'jhon@hotmail.com', '2016-05-11', '');
INSERT INTO clients (id, name, lastname, email, create_at, photo)values (23,'Alexis', 'Lopez', 'jhon@hotmail.com', '2016-05-11', '');
INSERT INTO clients (id, name, lastname, email, create_at, photo)values (24,'Alejandra', 'Santiago', 'jhon@hotmail.com', '2016-05-11', '');
INSERT INTO clients (id, name, lastname, email, create_at, photo)values (25,'Jhon', 'Hernandez', 'jhon@hotmail.com', '2016-05-11', '');



INSERT INTO products (name, amount, create_at)values ('Panasonic Pantall LCD', 2323, NOW());
INSERT INTO products (name, amount, create_at)values ('Sony Pantall 62', 25000, NOW());
INSERT INTO products (name, amount, create_at)values ('Apple 11', 18000, NOW());
INSERT INTO products (name, amount, create_at)values ('Apple Watch S4', 7000, NOW());
INSERT INTO products (name, amount, create_at)values ('HP Notebook Pa', 13000, NOW());
INSERT INTO products (name, amount, create_at)values ('Apple 8 Plus', 10000, NOW());
INSERT INTO products (name, amount, create_at)values ('Camara Sony', 6000, NOW());
INSERT INTO products (name, amount, create_at)values ('Chair Pro', 3000, NOW());
INSERT INTO products (name, amount, create_at)values ('Printer HP', 2000, NOW());

INSERT INTO invoices (description, observation, client_id, create_at) VALUES ('Invoices of work things', null, 1, now());
INSERT INTO items_invoice (quantity, Invoice_id, product_id) VALUES (3, 1, 5);
INSERT INTO items_invoice (quantity, Invoice_id, product_id) VALUES (2, 1, 9);
INSERT INTO items_invoice (quantity, Invoice_id, product_id) VALUES (2, 1, 8);

INSERT INTO invoices (description, observation, client_id, create_at) VALUES ('Invoices of Intretaiment', null, 1, now());
INSERT INTO items_invoice (quantity, Invoice_id, product_id) VALUES (4, 2, 2);
