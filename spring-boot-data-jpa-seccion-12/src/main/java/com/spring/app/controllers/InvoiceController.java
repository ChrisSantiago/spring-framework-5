package com.spring.app.controllers;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.spring.app.models.entity.Client;
import com.spring.app.models.entity.Invoice;
import com.spring.app.models.entity.InvoiceItem;
import com.spring.app.models.entity.Product;
import com.spring.app.models.service.IClientService;

@Controller
@RequestMapping("/invoice")
@SessionAttributes("invoice")
public class InvoiceController {

	@Autowired
	private IClientService clientService;

	private final Logger LOG = LoggerFactory.getLogger(getClass());

	@GetMapping("/show/{id}")
	public String showInvoice(@PathVariable(value = "id") Long id, Model model, RedirectAttributes flash) {
		Invoice invoice = clientService.fetchByIdWithClientWithInvoiceItemWithProduct(id);// clientService.findInvoiceById(id);
		if (invoice == null) {
			flash.addAttribute("error", "We are sorry!, We can not find the invoice in our data");
			return "redirect:/list";
		}
		model.addAttribute("title", "Invoice: ".concat(invoice.getDescription()));
		model.addAttribute("invoice", invoice);
		return "invoices/show-invoice";
	}

	@GetMapping("/form/{clientId}")
	public String create(@PathVariable(value = "clientId") Long clientId, Model model, RedirectAttributes flash) {
		Client client = clientService.findOne(clientId);
		if (client == null) {
			flash.addAttribute("error", "Opps, We could not find an user");
			return "redirect:/list";
		}
		Invoice invoice = new Invoice();
		invoice.setClient(client);

		model.addAttribute("invoice", invoice);
		model.addAttribute("title", "Make an invoice");
		return "invoices/form";
	}

	@GetMapping(value = "/loand-products/{term}", produces = "application/json")
	public @ResponseBody List<Product> loandProducts(@PathVariable String term) {
		return clientService.findByName(term);
	}

	@PostMapping("/form")
	public String save(@Valid Invoice invoice, BindingResult result, Model model,
			@RequestParam(name = "item_id[]", required = false) Long[] itemId,
			@RequestParam(name = "quantity[]", required = false) Integer[] quantities, RedirectAttributes flash,
			SessionStatus status) {

		if (result.hasErrors()) {
			model.addAttribute("title", "Make an invoice");
			return "invoices/form";
		}

		if (itemId == null || itemId.length == 0) {
			model.addAttribute("title", "Make an invoice");
			model.addAttribute("error", "Error: The invoice must have items");
			return "invoices/form";
		}

		for (int i = 0; i < itemId.length; i++) {
			Product product = clientService.findProductoById(itemId[i]);

			InvoiceItem invoiceItem = new InvoiceItem();
			invoiceItem.setQuantity(quantities[i]);
			invoiceItem.setProduct(product);
			invoice.addInvoiceItem(invoiceItem);

			LOG.info("ID: " + itemId[i].toString() + ", qunatity: " + quantities[i].toString());
		}

		clientService.saveInvoice(invoice);

		status.setComplete();
		flash.addFlashAttribute("success", "Invoice has been successfully created");

		return "redirect:/show/" + invoice.getClient().getId();
	}

	@GetMapping("/delete/{id}")
	public String delete(@PathVariable(value = "id") Long id, RedirectAttributes flash) {
		Invoice invoice = clientService.findInvoiceById(id);
		if (invoice != null) {
			clientService.deleteInvoice(id);
			flash.addFlashAttribute("success", "The invoice was deleted");
			return "redirect:/show/" + invoice.getClient().getId();
		}
		flash.addFlashAttribute("error",
				"We can not find the invoice in our datebase, it was not possible to delete it");
		return "redirect:/list";
	}

}
