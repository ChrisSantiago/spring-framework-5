package com.spring.app.models.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.spring.app.models.entity.Client;

@SuppressWarnings("unused")
public interface IClientDao extends PagingAndSortingRepository<Client, Long> {
	/*
	 * public List<Client> findAll();
	 * 
	 * public void save(Client client);
	 * 
	 * public Client findOne(Long id);
	 * 
	 * public void delete(Long id);
	 */

	@Query("select c from Client c left join fetch c.invoices i where c.id = ?1")
	public Client fetchByIdWithInvoices(Long id);
}
