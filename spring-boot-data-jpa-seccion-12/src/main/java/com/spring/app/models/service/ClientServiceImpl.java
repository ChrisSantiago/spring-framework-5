package com.spring.app.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.app.models.dao.IClientDao;
import com.spring.app.models.dao.IInvoiceDao;
import com.spring.app.models.dao.IProductDao;
import com.spring.app.models.entity.Client;
import com.spring.app.models.entity.Invoice;
import com.spring.app.models.entity.Product;

@Service
public class ClientServiceImpl implements IClientService {

	@Autowired
	private IClientDao clientDao;

	@Autowired
	private IProductDao productDao;

	@Autowired
	private IInvoiceDao invoiceDao;

	@Transactional(readOnly = true)
	@Override
	public List<Client> findAll() {
		return (List<Client>) clientDao.findAll();
	}

	@Transactional(readOnly = true)
	@Override
	public Client findOne(Long id) {
		return clientDao.findById(id).orElse(null);
	}

	@Override
	@Transactional(readOnly = true)
	public Client fetchByIdWithInvoices(Long id) {
		return clientDao.fetchByIdWithInvoices(id);
	}

	@Transactional
	@Override
	public void save(Client client) {
		clientDao.save(client);
	}

	@Transactional
	@Override
	public void delete(Long id) {
		clientDao.deleteById(id);

	}

	@Transactional(readOnly = true)
	@Override
	public Page<Client> findAll(Pageable pageable) {
		return clientDao.findAll(pageable);
	}

	@Transactional(readOnly = true)
	@Override
	public List<Product> findByName(String term) {
		// return productDao.findByName(term);
		return productDao.findByNameLikeIgnoreCase("%" + term + "%");
	}

	@Override
	@Transactional
	public void saveInvoice(Invoice invoice) {
		invoiceDao.save(invoice);
	}

	@Override
	@Transactional(readOnly = true)
	public Product findProductoById(Long id) {
		return productDao.findById(id).orElse(null);
	}

	@Override
	@Transactional(readOnly = true)
	public Invoice findInvoiceById(Long id) {
		return invoiceDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public void deleteInvoice(Long id) {
		invoiceDao.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Invoice fetchByIdWithClientWithInvoiceItemWithProduct(Long id) {
		return invoiceDao.fetchByIdWithClientWithInvoiceItemWithProduct(id);
	}

}
