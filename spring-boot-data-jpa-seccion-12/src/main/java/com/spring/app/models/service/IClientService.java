package com.spring.app.models.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.spring.app.models.entity.Client;
import com.spring.app.models.entity.Invoice;
import com.spring.app.models.entity.Product;

public interface IClientService {

	public List<Client> findAll();

	public Page<Client> findAll(Pageable pageable);

	public void save(Client client);

	public Client findOne(Long id);

	public Client fetchByIdWithInvoices(Long id);

	public void delete(Long id);

	public List<Product> findByName(String term);

	public void saveInvoice(Invoice invoice);

	public Product findProductoById(Long id);

	public Invoice findInvoiceById(Long id);

	public void deleteInvoice(Long id);

	public Invoice fetchByIdWithClientWithInvoiceItemWithProduct(Long id);
}
