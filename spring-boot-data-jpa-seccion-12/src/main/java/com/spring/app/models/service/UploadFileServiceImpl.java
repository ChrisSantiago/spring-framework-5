package com.spring.app.models.service;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

@Service
public class UploadFileServiceImpl implements IUploadFileService {

	private final Logger log = LoggerFactory.getLogger(getClass());

	private final static String UPLOADS_PATH = "uploads";

	@Override
	public Resource load(String filename) throws MalformedURLException {
		Path pathPhoto = getPath(filename);
		log.info("pathPhoto: " + pathPhoto);
		Resource resource = null;
		resource = new UrlResource(pathPhoto.toUri());
		if (!resource.exists() || !resource.isReadable()) {
			throw new RuntimeException("Error: no se puede cargar la imagen" + pathPhoto.toString());
		}

		return resource;
	}

	@Override
	public String copy(MultipartFile file) throws IOException {

		// Con la carpeta upload
		// Path pathResources = Paths.get("src//main//resources//static/uploads");
		// String rootPath = pathResources.toFile().getAbsolutePath();

		// Con un directorio fuera del proyecto
		// String rootPath = "C://Temp//uploads";

		// Directorio absoluto y directorio externo
		String uniqueFilename = UUID.randomUUID().toString() + "_" + file.getOriginalFilename();
		Path rooPath = getPath(uniqueFilename);
		log.info("rootPath: " + rooPath);

		// no Absoluto
		/*
		 * byte[] bytes = photo.getBytes(); Path completePath = Paths.get(rootPath +
		 * "//" + photo.getOriginalFilename()); Files.write(completePath, bytes);
		 */
		Files.copy(file.getInputStream(), rooPath);

		return uniqueFilename;
	}

	@Override
	public boolean delete(String filename) {
		Path rootPath = getPath(filename);
		File file = rootPath.toFile();

		if (file.exists() && file.canRead()) {
			if (file.delete()) {
				return true;
			}
		}
		return false;
	}

	public Path getPath(String filename) {
		return Paths.get(UPLOADS_PATH).resolve(filename).toAbsolutePath();
	}

	@Override
	public void deleteAll() {
		FileSystemUtils.deleteRecursively(Paths.get(UPLOADS_PATH).toFile());
	}

	@Override
	public void init() throws IOException {
		Files.createDirectory(Paths.get(UPLOADS_PATH));
	}

}
