package com.spring.app.models.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.spring.app.models.entity.Invoice;

public interface IInvoiceDao extends CrudRepository<Invoice, Long> {

	@Query("select i from Invoice i join fetch i.client c join fetch i.items l join fetch l.product where i.id = ?1")
	public Invoice fetchByIdWithClientWithInvoiceItemWithProduct(Long id);
}
