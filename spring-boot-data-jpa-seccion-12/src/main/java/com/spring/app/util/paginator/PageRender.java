package com.spring.app.util.paginator;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Page;

public class PageRender<T> {

	private String url;

	private Page<T> page;

	private int totalPages;

	private int numberOfItemsForPages;

	private int currentPage;

	List<PageItem> pages;

	public PageRender(String url, Page<T> page) {
		this.url = url;
		this.page = page;
		this.pages = new ArrayList<PageItem>();

		numberOfItemsForPages = page.getSize();
		totalPages = page.getTotalPages();
		currentPage = page.getNumber() + 1;

		int fromPage;
		int toPage;
		if (totalPages <= numberOfItemsForPages) {
			fromPage = 1;
			toPage = totalPages;
		} else {
			if (currentPage <= numberOfItemsForPages / 2) {
				fromPage = 1;
				toPage = totalPages;
			} else if (currentPage >= totalPages - numberOfItemsForPages / 2) {
				fromPage = totalPages - numberOfItemsForPages + 1;
				toPage = numberOfItemsForPages;
			} else {
				fromPage = currentPage - numberOfItemsForPages / 2;
				toPage = numberOfItemsForPages;
			}
		}

		for (int i = 0; i < toPage; i++) {
			this.pages.add(new PageItem(fromPage + i, currentPage == fromPage + i));
		}

	}

	public String getUrl() {
		return url;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public List<PageItem> getPages() {
		return pages;
	}
	
	public boolean isFirst() {
		return page.isFirst();
	}
	
	public boolean isLast() {
		return page.isLast();
	}
	
	public boolean isHasNext() {
		return page.hasNext();
	}
	
	public boolean isHasPrevious() {
		return page.hasPrevious();
	}

}
