package com.spring.controllers;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.spring.domain.Country;
import com.spring.domain.Role;
import com.spring.domain.User;
import com.spring.editors.PropertyCountryEditor;
import com.spring.editors.PropertyRoleEditor;
import com.spring.editors.UpperCaseNameEditor;
import com.spring.services.ICountryService;
import com.spring.services.IRoleService;
import com.spring.validation.UserValidator;

@Controller
@SessionAttributes("user")
public class FormController {

	@Autowired
	private UserValidator validator;

	@Autowired
	private ICountryService countryService;

	@Autowired
	private PropertyCountryEditor countryEditor;

	@Autowired
	private IRoleService roleService;

	@Autowired
	private PropertyRoleEditor propertyRoleEditor;

	@InitBinder
	public void InitBinder(WebDataBinder binder) {
		binder.addValidators(validator);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat.setLenient(false);

		binder.registerCustomEditor(Date.class, "dateBirthday", new CustomDateEditor(dateFormat, false));

		binder.registerCustomEditor(String.class, "name", new UpperCaseNameEditor());
		binder.registerCustomEditor(String.class, "lastname", new UpperCaseNameEditor());

		binder.registerCustomEditor(Country.class, "country", countryEditor);
		binder.registerCustomEditor(Role.class, "roles", propertyRoleEditor);
	}

	// PAIS: List de objeto Pais
	@ModelAttribute("listCountries")
	public List<Country> listCountries() {
		return countryService.fillCountries();
	}

	// ROL: List de objeto Rol
	@ModelAttribute("roleList")
	public List<Role> roleList() {
		return roleService.fillRole();
	}

	// GENERO List de String
	@ModelAttribute("gender")
	public List<String> gender() {
		return Arrays.asList("Male", "Femele");
	}

	@GetMapping("/form")
	public String form(Model model) {
		User user = new User();
		user.setId("12.223.123-G");
		user.setName("Christofer");
		user.setLastname("Santiago");
		user.setEnable(true);
		user.setSecretValue("This is a secret value");
		user.setCountry(new Country(5, "FR", "France"));
		user.setRoles(Arrays.asList(new Role(1, "Administrator", "ROLE_ADMIN")));
		model.addAttribute("title", "Form of User");
		model.addAttribute("user", user);
		return "form";
	}

	@PostMapping("/form")
	public String processing(@Valid User user, BindingResult result, Model model) {
		if (result.hasErrors()) {
			model.addAttribute("title", "Result of form");
			return "form";
		}
		return "redirect:/show";
	}

	@GetMapping("/show")
	public String show(@SessionAttribute(name = "user", required = false) User user, Model model,
			SessionStatus status) {
		if(user == null) {
			return "redirect:/form";
		}
		model.addAttribute("title", "Result of form");
		status.setComplete();
		return "view-result";
	}

}
