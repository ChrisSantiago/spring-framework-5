package com.spring.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.websocket.server.ServerEndpoint;

import org.springframework.stereotype.Service;

import com.spring.domain.Country;

@Service
public class CountryServiceImpl implements ICountryService{
	
	private List<Country> countries;
	
	public CountryServiceImpl() {
		countries = Arrays.asList(
				new Country(1, "MX", "Mexico"), 
				new Country(2, "EU", "EEUU"), 
				new Country(3, "IR", "Irland"),
				new Country(4, "NL", "Hollan"), 
				new Country(5, "FR", "France"));
	}

	@Override
	public List<Country> fillCountries() {
		return countries;
	}

	@Override
	public Country getCountryById(Integer id) {
		for(Country country : countries) {
			if(id == country.getId()) {
				return country;
			}
		}
		return null;
	}
	

}
