package com.spring.services;

import java.util.List;

import com.spring.domain.Country;

public interface ICountryService {
	
	public List<Country> fillCountries();
	
	public Country getCountryById(Integer id);

}
