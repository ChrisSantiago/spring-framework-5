package com.spring.services;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import com.spring.domain.Role;

@Service
public class RoleServiceImpl implements IRoleService {

	private List<Role> roles;

	public RoleServiceImpl() {
		roles = Arrays.asList(new Role(1, "Administrator", "ROLE_ADMIN"), new Role(2, "User", "ROLE_USER"),
				new Role(3, "Moderator", "ROLE_MODERATOR"));
	}

	@Override
	public List<Role> fillRole() {
		return roles;
	}

	@Override
	public Role getRoleById(Integer id) {

		for (Role role : roles) {
			if (id == role.getId()) {
				return role;
			}
		}
		return null;
	}

}
