package com.spring.services;

import java.util.List;

import com.spring.domain.Role;

public interface IRoleService {
	
	public List<Role> fillRole();
	
	public Role getRoleById(Integer id);

}
