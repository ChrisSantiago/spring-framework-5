package com.spring.interceptors;

import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

@Component("loadTimeInterceptor")
public class LoadTimeInterceptor implements HandlerInterceptor {

	private static final Logger logger = LoggerFactory.getLogger(LoadTimeInterceptor.class);

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {

		if(request.getMethod().equalsIgnoreCase("post")) {
			return;
		}

		long endTime = System.currentTimeMillis();
		long startTime = (Long) request.getAttribute("startTime");
		long elapsedTime = endTime - startTime;

		if (handler instanceof HandlerMethod && modelAndView != null) {
			modelAndView.addObject("elapsedTime", elapsedTime);
		}

		logger.info("Elepsed time: " + elapsedTime + " Miliseconds");
		logger.info("LoadTimeInterceptor: postHandle() saliendo ...");
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		if(request.getMethod().equalsIgnoreCase("post")) {
			return true;
		}

		if (handler instanceof HandlerMethod) {
			HandlerMethod method = (HandlerMethod) handler;
			logger.info("Es un metodo del controllador: " + method.getMethod().getName());
		}

		logger.info("LoadTimeInterceptor: preHandle() entrando ...");
		long startTime = System.currentTimeMillis();
		request.setAttribute("startTime", startTime);

		logger.info("Start time: " + startTime + " Miliseconds");

		Random random = new Random();
		Integer later = random.nextInt(100);
		Thread.sleep(later);
		/*
		 * Redigir en caso que sea false
		response.sendRedirect(request.getContextPath().concat("/login"));
		return false;*/
		
		return true;
	}

}
