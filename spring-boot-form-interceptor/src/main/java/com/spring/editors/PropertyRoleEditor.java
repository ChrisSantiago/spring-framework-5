package com.spring.editors;

import java.beans.PropertyEditorSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spring.services.IRoleService;

@Component
public class PropertyRoleEditor extends PropertyEditorSupport{
	
	@Autowired
	private IRoleService roleService;

	@Override
	public void setAsText(String id) throws IllegalArgumentException {
		try {
			this.setValue(roleService.getRoleById(Integer.parseInt(id)));
		} catch (NumberFormatException e) {
			this.setValue(null);
		}
	}
	
	

}
