package com.spring.editors;

import java.beans.PropertyEditorSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spring.services.ICountryService;

@Component
public class PropertyCountryEditor extends PropertyEditorSupport {

	@Autowired
	private ICountryService countryService;

	@Override
	public void setAsText(String id) throws IllegalArgumentException {
		try {
			this.setValue(countryService.getCountryById(Integer.parseInt(id)));
		} catch (NumberFormatException e) {
			this.setValue(null);
		}
	}
}
