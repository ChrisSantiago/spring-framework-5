package com.spring.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.spring.domain.User;

@Component
public class UserValidator implements Validator {

	@Override
	public boolean supports(Class<?> arg0) {
		return User.class.isAssignableFrom(arg0);
	}

	@Override
	public void validate(Object arg0, Errors errors) {
		User user = (User) arg0;
		//Utilizando el mensaje que se encuntra en la clase usuario
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "NotEmpty.user.name");
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "require.user.name");
		
		/*
		//Alternativa
		if(user.getName().isEmpty()) {
			errors.rejectValue("name", "NotEmpty.user.name");
		}*/
		
		// No se utiliza ya que se esta creado una notacion propia @IdentifierRegex
		/*if(!user.getId().matches("[0-9]{2}[.][\\d]{3}[.][\\d]{3}[-][A-Z]{1}")) {
			errors.rejectValue("id", "pattern.user.id");
		}*/
	}

}
