package com.spring.domain;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class Country {
	
	private Integer id;
	
	//@NotEmpty
	private String code;
	
	private String value;

	
	public Country() {
		
	}
	
	public Country(Integer id, String code, String value) {
		this.id = id;
		this.code = code;
		this.value = value;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return this.id.toString();
	}
}
