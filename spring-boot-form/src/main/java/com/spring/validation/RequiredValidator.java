package com.spring.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.util.StringUtils;

public class RequiredValidator implements ConstraintValidator<Required, String> {

	@Override
	public boolean isValid(String value, ConstraintValidatorContext arg1) {
		if(value == null || /*value.isEmpty() || value.trim().isEmpty()*/ !StringUtils.hasText(value)) {
			return false;
		}
		return true;
	}

}
