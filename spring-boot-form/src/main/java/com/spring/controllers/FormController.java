package com.spring.controllers;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.spring.domain.Country;
import com.spring.domain.Role;
import com.spring.domain.User;
import com.spring.editors.PropertyCountryEditor;
import com.spring.editors.PropertyRoleEditor;
import com.spring.editors.UpperCaseNameEditor;
import com.spring.services.ICountryService;
import com.spring.services.IRoleService;
import com.spring.validation.UserValidator;

@Controller
@SessionAttributes("user")
public class FormController {

	@Autowired
	private UserValidator validator;

	@Autowired
	private ICountryService countryService;

	@Autowired
	private PropertyCountryEditor countryEditor;

	@Autowired
	private IRoleService roleService;

	@Autowired
	private PropertyRoleEditor propertyRoleEditor;

	@InitBinder
	public void InitBinder(WebDataBinder binder) {
		binder.addValidators(validator);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat.setLenient(false);
		// binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat,
		// false));
		// Un campo especifico
		binder.registerCustomEditor(Date.class, "dateBirthday", new CustomDateEditor(dateFormat, false));

		binder.registerCustomEditor(String.class, "name", new UpperCaseNameEditor());
		binder.registerCustomEditor(String.class, "lastname", new UpperCaseNameEditor());

		binder.registerCustomEditor(Country.class, "country", countryEditor);
		binder.registerCustomEditor(Role.class, "roles", propertyRoleEditor);
	}

	// PAIS: Arreglo de String
	@ModelAttribute("countries")
	public List<String> countries() {
		return Arrays.asList("Mexico", "EEUU", "Irland", "Hollan", "France");
	}

	// PAIS: List de objeto Pais
	@ModelAttribute("listCountries")
	public List<Country> listCountries() {
		return countryService.fillCountries();
	}

	// PAIS: Map de String
	@ModelAttribute("countriesMap")
	public Map<String, String> countriesMap() {
		Map<String, String> countries = new HashMap<String, String>();
		countries.put("MX", "Mexico");
		countries.put("EU", "EEUU");
		countries.put("IR", "Irland");
		countries.put("NL", "Hollan");
		countries.put("FR", "France");
		return countries;
	}

	// ROL: List de objeto Rol
	@ModelAttribute("roleList")
	public List<Role> roleList() {
		return roleService.fillRole();
	}

	// ROL: List de String
	@ModelAttribute("roleStringList")
	public List<String> roleStringList() {
		return Arrays.asList("ROLE_ADMIN", "ROLE_USER", "ROLE_MODERATOR");
	}

	// ROL: Map de String
	@ModelAttribute("roleMapList")
	public Map<String, String> roleMapList() {
		Map<String, String> countries = new HashMap<String, String>();
		countries.put("ROLE_ADMIN", "Administrator");
		countries.put("ROLE_USER", "User");
		countries.put("ROLE_MODERATOR", "Moderator");
		return countries;
	}

	// GENERO List de String
	@ModelAttribute("gender")
	public List<String> gender() {
		return Arrays.asList("Male", "Femele");
	}

	@GetMapping("/form")
	public String form(Model model) {
		User user = new User();
		user.setId("12.223.123-G");
		user.setName("Christofer");
		user.setLastname("Santiago");
		user.setEnable(true);
		user.setSecretValue("This is a secret value");
		user.setCountry(new Country(5, "FR", "France"));
		user.setRoles(Arrays.asList(new Role(1, "Administrator", "ROLE_ADMIN")));
		model.addAttribute("title", "Form of User");
		model.addAttribute("user", user);
		return "form";
	}

	@PostMapping("/form")
	public String processing(@Valid /* @ModelAttribute("user") */ User user, BindingResult result, Model model
	/* ,SessionStatus status */
	// @RequestParam(name = "username") String username,
	// @RequestParam String password,
	// @RequestParam String email) {
	) {
		// Sin Utilizar InitBinder
		// validator.validate(user, result);

		// model.addAttribute("title", "Result of form");
		if (result.hasErrors()) {
			model.addAttribute("title", "Result of form");
			/*
			 * Map<String, String> errors = new HashMap<>();
			 * result.getFieldErrors().forEach(err ->{ errors.put(err.getField(),
			 * "El campo ".concat(err.getField()).concat(" ").concat(err.getDefaultMessage()
			 * )); }); model.addAttribute("error", errors);
			 */
			return "form";
		}
		/*
		 * User user = new User(); user.setUsername(username);
		 * user.setPassword(password); user.setEmail(email);
		 */
		/*
		 * REDIRCT model.addAttribute("user", user); status.setComplete(); return
		 * return "view-result";
		 */
		return "redirect:/show";
	}

	@GetMapping("/show")
	public String show(@SessionAttribute(name = "user", required = false) User user, Model model,
			SessionStatus status) {
		if(user == null) {
			return "redirect:/form";
		}
		model.addAttribute("title", "Result of form");
		status.setComplete();
		return "view-result";
	}

}
