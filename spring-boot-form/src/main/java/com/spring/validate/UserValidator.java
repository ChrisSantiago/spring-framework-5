package com.spring.validate;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.spring.domain.User;

public class UserValidator implements Validator {

	@Override
	public boolean supports(Class<?> arg0) {
		return User.class.isAssignableFrom(arg0);
	}

	@Override
	public void validate(Object arg0, Errors errors) {
		User user = (User) arg0;
		ValidationUtils.rejectIfEmpty(errors, "name", "NotEmpty.user.name");
		/*
		//Alternativa
		if(user.getName().isEmpty()) {
			errors.rejectValue("name", "NotEmpty.user.name");
		}*/
		
		if(!user.getEmail().matches("[0-9]{2}[.][\\\\d]{3}[.][\\\\d]{3}[-][A-Z]{1}")) {
			errors.rejectValue("id", "pattern.user.id");
		}
	}

}
