package com.spring.controllers;

import javax.websocket.server.PathParam;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/variables")
public class EjemploVariablesRutaController {

	@GetMapping("/")
	public String index(Model model) {
		model.addAttribute("title", "Enviar parametros de la ruta (@PathVariable)");
		return "variables/index";
	}
	
	@GetMapping("/string/{texto}")
	public String variables(@PathVariable(name = "texto") String textoOtro, Model model) {
		model.addAttribute("title", "Recibir parametros de la ruta (@PathVariable)");
		model.addAttribute("result", "The text which was sent: " + textoOtro);
		return "variables/ver";
	}
	
	@GetMapping("/string/{texto}/{numero}")
	public String variables(@PathVariable(name = "texto") String textoOtro,@PathVariable Integer numero,  Model model) {
		model.addAttribute("title", "Recibir parametros de la ruta (@PathVariable)");
		model.addAttribute("result", "The text which was sent: " + textoOtro +
				"and the number is: " + numero);
		return "variables/ver";
	}
}
