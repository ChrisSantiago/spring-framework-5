package com.spring.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.models.Usuario;

@Controller
@RequestMapping("/app")
public class IndexController {
	
	@Value("${text.indexcontroller.index.title}")
	private String textIndex;
	
	@Value("${text.indexcontroller.profile.title}")
	private String textProfile;
	
	@Value("${text.indexcontroller.list.title}")
	private String textList;
	
	@GetMapping({"/index", "/", "/home", ""})
	public String index(Model model) {
		model.addAttribute("title", textIndex);
		
		return "index";
	}
	
	@RequestMapping("/profile")
	public String profile(Model model) {
		Usuario usuario = new Usuario();
		usuario.setNombre("Christofer");
		usuario.setApellido("Santiago");
		usuario.setEmail("chris@hotmail.com");
		model.addAttribute("title", textProfile + " ".concat(usuario.getNombre()));
		model.addAttribute("usuario", usuario);
		
		return "profile";
	}
	
	
	@RequestMapping("/list")
	public String list(Model model) {
//		List<Usuario> usuarios = new ArrayList<Usuario>() {{
//			add(new Usuario("Alejandro", "Santiago","ale@hotmail.com"));
//			add(new Usuario("Sonia", "Garcia", "sonia@hotmail.com"));
//			add(new Usuario("Jhonatan", "Santiago", "jhon@hotmail.com"));
//		}};
//		model.addAttribute("users", usuarios);
		model.addAttribute("title", textList);
	
		
		return "list";
	}
	
	//@ModelAttribute -> pasa atributos a la vista mediante el return y la etiqueta
	//Se parasaran a todas las vistas
	@ModelAttribute("users")
	public List<Usuario> sendUsers(){
		List<Usuario> usuarios = new ArrayList<Usuario>() {{
			add(new Usuario("Alejandro", "Santiago","ale@hotmail.com"));
			add(new Usuario("Sonia", "Garcia", "sonia@hotmail.com"));
			add(new Usuario("Jhonatan", "Santiago", "jhon@hotmail.com"));
		}};
		
		return usuarios;
	}

}
