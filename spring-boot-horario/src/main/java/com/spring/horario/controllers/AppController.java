package com.spring.horario.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AppController {
	
	@Value("${config.schedule.open}")
	private Integer open;
	
	@Value("${config.schedule.closed}")
	private Integer closed;
	
	@GetMapping({"/","/index"})
	public String index(Model model) {
		model.addAttribute("title", "Welcome to the schedule of clients");
		return "index";
	}
	
	@GetMapping("/closed")
	public String closed(Model model) {
		StringBuilder message = new StringBuilder("It is closed, please you migth visit us at ");
		message.append(open);
		message.append(" to ");
		message.append(closed);
		message.append("hrs. Thank you");
		
		model.addAttribute("title", "Out of time");
		model.addAttribute("message", message.toString());
		return "closed";
	}

}
