package com.curso.springboot.di.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.curso.springboot.di.domain.Bill;

@Controller
@RequestMapping("/bill")
public class BillController {
	
	@Autowired
	private Bill bill;

	@GetMapping("/show")
	public String show(Model model) {
		model.addAttribute("title", "Example of bill using DI");
		model.addAttribute("bill", bill);
		return "bill/show";
	}
}
