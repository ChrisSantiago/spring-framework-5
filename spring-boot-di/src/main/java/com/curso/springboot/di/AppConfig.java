package com.curso.springboot.di;

import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.curso.springboot.di.domain.ItemBill;
import com.curso.springboot.di.domain.Product;
import com.curso.springboot.di.service.IServicio;
import com.curso.springboot.di.service.MiServicio;
import com.curso.springboot.di.service.MiServicioComplejo;

@Configuration
public class AppConfig {
	
	@Bean("miServicioSimple")
	public IServicio registrarMiServicio() {
		return new MiServicio();
	}
	
	@Primary
	@Bean("miServicioComplejo")
	public IServicio registrarMiServicioComplejo() {
		return new MiServicioComplejo();
	} 
	
	@Bean("ItemBill")
	public List<ItemBill> registerItems(){
		Product product1 = new Product("Camara Sony", 100);
		Product product2 = new Product("IPhone 11", 15000);
		ItemBill item1 = new ItemBill(product1, 2);
		ItemBill item2 = new ItemBill(product2, 4);
		
		return Arrays.asList(item1, item2);
	}
	
	@Bean("OfficeItemBill")
	public List<ItemBill> registerOfficeItems(){
		Product product1 = new Product("Lapotop HP", 400);
		Product product2 = new Product("Telephone", 50);
		Product product3 = new Product("Chair", 10);
		Product product4 = new Product("Office table", 50);
		ItemBill item1 = new ItemBill(product1, 2);
		ItemBill item2 = new ItemBill(product2, 4);
		ItemBill item3 = new ItemBill(product3, 5);
		ItemBill item4 = new ItemBill(product4, 5);
		
		return Arrays.asList(item1, item2, item3, item4);
	}

}
