package com.curso.springboot.di.domain;

public class Product {

	private String name;

	private Integer amount;
	
	public Product(String name, Integer amount) {
		this.name = name;
		this.amount = amount;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "Product [name=" + name + ", amount=" + amount + "]";
	}

}
