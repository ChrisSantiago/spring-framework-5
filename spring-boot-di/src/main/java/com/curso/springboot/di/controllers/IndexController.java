package com.curso.springboot.di.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.curso.springboot.di.service.IServicio;
import com.curso.springboot.di.service.MiServicio;

@Controller
public class IndexController {

	@Autowired
	//@Qualifier("miServicioComplejo")
	private IServicio servicio;
	
	
	//DI Constructor
	/*
	@Autowired
	public IndexController(IServicio servicio) {
		this.servicio = servicio;
	}*/

	@GetMapping({"/", "", "/index"})
	public String index(Model model) {
		model.addAttribute("objeto", servicio.operacion());
		return "index";
	}

	public IServicio getServicio() {
		return servicio;
	}

	//DI en el setters
	/*
	@Autowired
	public void setServicio(IServicio servicio) {
		this.servicio = servicio;
	}*/
	
	
}
