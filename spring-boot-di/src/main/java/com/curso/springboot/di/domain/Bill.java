package com.curso.springboot.di.domain;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Bill {

	@Value("${bill.description}")
	private String description;

	@Autowired
	private Client client;

	@Autowired
	@Qualifier("OfficeItemBill")
	private List<ItemBill> items;
	
	@PostConstruct
	public void init() {
		client.setName(client.getName().concat(" ").concat("Alejandro"));
		description =  description.concat(" Of Client ").concat(client.getName());
		//System.out.println("PostContruct ".concat(client.getName()));
	}
	
	@PreDestroy
	public void destroy() {
		System.out.print("The bill has been destroyed");
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public List<ItemBill> getItems() {
		return items;
	}

	public void setItems(List<ItemBill> items) {
		this.items = items;
	}

	@Override
	public String toString() {
		return "Bill [description=" + description + ", client=" + client + ", items=" + items + "]";
	}

}
