package com.curso.springboot.di.service;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;


//@Primary
//@Component("miServicioSimple")
public class MiServicio implements IServicio {

	@Override
	public String operacion() {
		return "Ejecutamdo una tarea importante simple...";
	}
	

}
