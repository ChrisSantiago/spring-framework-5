package com.spring.app.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.app.models.dao.IClientDao;
import com.spring.app.models.entity.Client;

@Service
public class ClientServiceImpl implements IClientService {

	@Autowired
	private IClientDao clientDao;

	@Transactional(readOnly = true)
	@Override
	public List<Client> findAll() {
		return (List<Client>) clientDao.findAll();
	}

	@Transactional(readOnly = true)
	@Override
	public Client findOne(Long id) {
		return clientDao.findById(id).orElse(null);
	}

	@Transactional
	@Override
	public void save(Client client) {
		clientDao.save(client);
	}

	@Transactional
	@Override
	public void delete(Long id) {
		clientDao.deleteById(id);

	}

}
