package com.spring.app.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.spring.app.models.entity.Client;
import com.spring.app.models.service.IClientService;

@Controller
@SessionAttributes("client")
public class ClientController {

	@Autowired
	private IClientService clientService;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String list(Model model) {
		model.addAttribute("title", "Client Lists");
		model.addAttribute("clients", clientService.findAll());
		return "list";
	}

	@RequestMapping(value = "/form")
	public String registerClient(Model model) {
		model.addAttribute("client", new Client());
		model.addAttribute("title", "Client Form");
		return "form";
	}

	@RequestMapping(value = "/form", method = RequestMethod.POST)
	public String save(@Valid Client client, BindingResult result, Model model, SessionStatus status) {
		if (result.hasErrors()) {
			model.addAttribute("title", "Opps Error in the form");
			return "form";
		}
		clientService.save(client);
		status.setComplete();
		return "redirect:list";
	}

	@RequestMapping(value = "/form/{id}", method = RequestMethod.GET)
	public String update(@PathVariable("id") Long id, Model model) {
		Client client = null;
		if (id > 0) {
			client = clientService.findOne(id);
		} else {
			return "redirect:/list";
		}

		model.addAttribute("client", client);
		model.addAttribute("title", "Edit Client");

		return "form";
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public String delete(@PathVariable("id") Long id) {
		if (id > 0) {
			clientService.delete(id);
		}
		return "redirect:/list";
	}

}
