package com.spring.error.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.spring.error.error.UserNotFoundException;
import com.spring.error.models.domain.User;
import com.spring.error.services.IUserService;

@Controller
public class AppController {
	
	@Autowired
	private IUserService userService;
	
	@GetMapping({"index"})
	public String index() {
		//Integer result = 100/0;
		Integer result = Integer.parseInt("12dkh");
		return "index";
	}
	
	@GetMapping("/viewer/{id}")
	public String showUser(@PathVariable Integer id, Model model) {
		/*
		//Validacion normal, sin usar optional hava 8
		User user = userService.getById(id);
		if(user == null) {
			throw new UserNotFoundException(id.toString());
		}*/
		//Usando Optional java 8
		User user = userService.getByIdOptional(id).orElseThrow(() -> new UserNotFoundException(id.toString()));
		model.addAttribute("user", user);
		model.addAttribute("title", "Detail of user: ".concat(user.getName()));
		return "viewer";
	}

}
