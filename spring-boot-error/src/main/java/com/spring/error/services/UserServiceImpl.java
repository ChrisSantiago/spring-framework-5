package com.spring.error.services;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.spring.error.models.domain.User;

@Service
public class UserServiceImpl implements IUserService {

	private List<User> users;

	public UserServiceImpl() {
		users = Arrays.asList(new User(1, "Kristen", "Martinez"), new User(2, "Lucy", "Arriaga"),
				new User(3, "Luis", "Hernandez"), new User(4, "Emily", "Garcia"), new User(5, "Ricardo", "Solis"),
				new User(6, "Miranda", "Guierrez"), new User(7, "Patricia", "Martinez"));
	}

	@Override
	public List<User> getListUser() {
		return this.users;
	}

	@Override
	public User getById(Integer id) {
		for (User user : users) {
			if (user.getId().equals(id)) {
				return user;
			}
		}

		return null;
	}

	@Override
	public Optional<User> getByIdOptional(Integer id) {
		User user = this.getById(id);
		return Optional.ofNullable(user);
	}

}
