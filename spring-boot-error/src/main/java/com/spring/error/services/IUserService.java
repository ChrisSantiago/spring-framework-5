package com.spring.error.services;

import java.util.List;
import java.util.Optional;

import com.spring.error.models.domain.User;

public interface IUserService {
	
	public List<User> getListUser();
	
	public User getById(Integer id);
	
	public Optional<User> getByIdOptional(Integer id);

}
