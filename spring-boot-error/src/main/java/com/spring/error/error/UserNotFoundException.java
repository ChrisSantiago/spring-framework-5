package com.spring.error.error;

public class UserNotFoundException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public UserNotFoundException(String id) {
		super("User: ".concat(id).concat(" can not found"));
	}
}
